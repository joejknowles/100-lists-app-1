import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders input", () => {
  const { getByAltText } = render(<App />);
  const input = getByAltText(/Add a todo/i);
  expect(input).toBeInTheDocument();
});
