import React from "react";
import styled from "styled-components";
import { TodoTextInput, focusTextInput } from "./TodoTextInput";
import { TodoCheckbox } from "./TodoCheckbox";
import { AddIcon, DeleteIcon } from "./Icons";

const Root = styled.div`
  display: flex;
  align-items: center;
`;

const Todo = (props) => {
  const { deleteTodo, value, id } = props;
  return (
    <Root>
      {value ? (
        <TodoCheckbox {...props} label={`checkbox for item "${value}"`} />
      ) : (
        <AddIcon onClick={() => focusTextInput(id)} />
      )}
      <TodoTextInput
        {...props}
        aria-label={
          value
            ? `text field for item "${value}"`
            : "text field for empty list item"
        }
      />
      {value && (
        <DeleteIcon
          onClick={deleteTodo}
          aria-label={`delete item "${value}"`}
        />
      )}
    </Root>
  );
};

export { Todo };
