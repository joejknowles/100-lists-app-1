import React from "react";
import styled, { css } from "styled-components";
import { colors } from "../colors";
import Add from "@material-ui/icons/Add";
import Delete from "@material-ui/icons/Close";

export const AddIcon = styled(Add)`
  color: ${colors.greys[7]};
  font-size: 1rem;
  margin-right: 1rem;
  &:hover {
    cursor: pointer;
  }
`;

export const DeleteContainer = styled.button`
  color: white;
  transition: color 0.2s linear;
  font-size: 1rem;
  height: 0;
  width: 0;
  padding: 0;
  border: none;
  overflow: hidden;
  outline: none;
  background: none;
  &:hover {
    cursor: pointer;
  }
  div:hover > &,
  input:focus ~ &,
  &:focus {
    height: 1.5rem;
    width: inherit;
    overflow: inherit;
    color: ${colors.greys[5]};
  }
  &:focus {
    color: ${colors.greys[7]};
    box-shadow: 1px 1px 4px ${colors.greys[6]};
  }
`;

export const DeleteIcon = (props) => (
  <DeleteContainer {...props}>
    <Delete />
  </DeleteContainer>
);

export const IconContainer = css`
  margin-right: 1rem;
`;
