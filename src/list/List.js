import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import { Todo } from "./Todo";
import { focusTextInput } from "./TodoTextInput";

const Root = styled.div``;

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const deleteWithId = (list, id) => list.filter((item) => item.id !== id);

const List = () => {
  const [todos, setTodos] = useState([]);
  const [nextId, setNextId] = useState(1);
  const prevTodos = usePrevious(todos);
  const lastTodo = todos[todos.length - 1];
  useEffect(() => {
    const newTodoAdded = prevTodos && todos.length === prevTodos.length + 1;
    if (newTodoAdded) {
      focusTextInput(lastTodo.id);
    }
  }, [todos, prevTodos, lastTodo]);

  console.log(...todos);
  return (
    <Root>
      {todos.map((todo, index) => (
        <Todo
          key={todo.id}
          {...todo}
          onChange={(e) =>
            setTodos(
              todos.map((t) => {
                if (t.id === todo.id) {
                  return {
                    ...todo,
                    value: e.target.value,
                  };
                }
                return t;
              })
            )
          }
          deleteTodo={() => setTodos(deleteWithId(todos, todo.id))}
          onBlur={() => !todo.value && setTodos(deleteWithId(todos, todo.id))}
          moveToNext={() => {
            const nextTodoId = todos[index + 1]?.id || nextId;
            todo.value && focusTextInput(nextTodoId);
          }}
        />
      ))}
      {(!lastTodo || lastTodo.value) && (
        <Todo
          id={nextId}
          onFocus={() => {
            setTodos((ts) => [...ts, { id: nextId, value: "" }]);
            setNextId(nextId + 1);
          }}
          placeholder={todos[0] ? null : "Add a todo..."}
          isInitialTodo={!todos[0]}
          alt="Add a todo"
        />
      )}
    </Root>
  );
};

export { List };
