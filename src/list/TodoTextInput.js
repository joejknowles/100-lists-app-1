import React from "react";
import styled from "styled-components";
import { colors } from "../colors";

const Input = styled.input`
  font-family: work sans;
  border: none;
  padding: 1rem;
  border-bottom: solid 1px white;
  transition: border-color 0.2s linear, color 0.2s linear;
  &:hover {
    border-bottom: solid 1px ${colors.greys[2]};
  }
  &:active,
  &:focus {
    border-bottom: solid 1px ${colors.greys[3]};
  }
  ${({ isInitialTodo }) =>
    isInitialTodo ? `border-bottom: solid 1px ${colors.greys[2]};` : ""}
  width: 508px;
  font-size: 1rem;
  outline: none;
  color: ${colors.greys[7]};
  &:focus {
    color: ${colors.greys[9]};
  }

  input:checked ~ & {
    text-decoration: line-through;
    color: ${colors.greys[5]};
  }
`;

const getTextInputId = (id) => `todo-${id}-textInput`;
export const focusTextInput = (id) =>
  document.getElementById(getTextInputId(id)).focus();

export const TodoTextInput = (props) => (
  <Input
    {...props}
    id={getTextInputId(props.id)}
    autoComplete={"off"}
    onKeyPress={(e) => {
      if (e.charCode === 13 && props.moveToNext) {
        props.moveToNext();
      }
    }}
  />
);
