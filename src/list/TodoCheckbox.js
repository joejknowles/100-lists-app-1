import React, { Fragment } from "react";
import styled from "styled-components";
import { colors } from "../colors";

const CheckboxLabel = styled.label`
  border: 1px solid ${colors.greys[7]};
  min-width: 1rem;
  height: 1rem;
  border-radius: 0.1rem;
  margin-left: 0.2rem;
  margin-right: 1rem;
  cursor: pointer;
  z-index: 1;
  input:focus + & {
    box-shadow: 1px 1px 3px ${colors.greys[5]};
    background-color: ${colors.greys[2]};
  }
  input:checked + & {
    background-color: ${colors.greys[7]};
  }
`;

const CheckboxInput = styled.input.attrs({ type: "checkbox" })`
  opacity: 0;
  position: absolute;
`;

const getCheckboxInputId = (id) => `todo-${id}-checkbox`;

export const TodoCheckbox = ({ id, label }) => (
  <Fragment>
    <CheckboxInput id={getCheckboxInputId(id)} />
    <CheckboxLabel aria-label={label} htmlFor={getCheckboxInputId(id)} />
  </Fragment>
);
