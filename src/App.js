import React from "react";
import styled, { createGlobalStyle } from "styled-components";
import { colors } from "./colors";
import { List } from "./list/List";

const StyleBody = createGlobalStyle`
  body, #root, html {
    height: 100%;
    margin: 0;
    font-family: work sans;
    font-size: 16px;
    background-color: ${colors.greys[2]};
  }
`;

const Root = styled.div`
  padding: 3rem 0;
`;

const Card = styled.div`
  margin: 0 auto;
  padding: 2rem 2rem;
  @media (min-width: 500px) {
    padding: 2rem 3rem;
  }
  background-color: white;
  width: 540px;
  max-width: 95%;
  box-sizing: border-box;
  box-shadow: 0.1rem 0.1rem 0.2rem ${colors.greys[5]},
    0.5rem 0.5rem 0.5rem ${colors.greys[3]};
  border-radius: 0.5rem;
`;

function App() {
  return (
    <Root>
      <StyleBody />
      <Card>
        <List />
      </Card>
    </Root>
  );
}

export default App;
